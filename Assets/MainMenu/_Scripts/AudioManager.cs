﻿using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    AnimationCurve volCurve;
    [SerializeField]
    AudioMixer mainMixer;
    [SerializeField]
    AudioClip[] mods;

    AudioSource musicSource;
    int maxMusicIndex;
    int musicIndex;

    private void Awake()
    {
        musicSource = GetComponentInChildren<Tag_MusicSource>()?.GetComponent<AudioSource>();

    }

    private void OnEnable()
    {
        MenuEvents.Instance.PrevTrack.AddListener(OnPrevTrack);
        MenuEvents.Instance.NextTrack.AddListener(OnNextTrack);
        MenuEvents.Instance.ChangeVolume.AddListener(OnChangeVolume);
        MenuEvents.Instance.PrintUnitValFromVolume.AddListener(PrintUnitValFromVolume);
    }

    private void Start()
    {

        maxMusicIndex = mods.Length - 1;
        musicSource.clip = mods[musicIndex];
        musicSource.Play();
    }

    void OnPrevTrack()
    {
        musicIndex--;
        if(musicIndex < 0)
        {
            musicIndex = maxMusicIndex;
        }

        StartNewTrack();
    }

    void OnNextTrack()
    {
        musicIndex++;
        if (musicIndex > maxMusicIndex)
        {
            musicIndex = 0;
        }

        StartNewTrack();
    }

    void StartNewTrack()
    {
        musicSource.Stop();
        musicSource.clip = mods[musicIndex];
        musicSource.Play();

        Debug.Log(musicSource.clip.name);
    }

    void OnChangeVolume(float val)
    {
        //float t = volCurve.Evaluate(val);

        float volume = Mathf.Lerp(-80, 0, val); 

        mainMixer.SetFloat("MusicVol", volume);
    }

    public void PrintUnitValFromVolume()
    {
        float vol = 0;
        mainMixer.GetFloat("MusicVol", out vol);

        vol = Utils.Map(vol, -80f, 0f, 0f, 1f);
        print(vol.ToString());
    }
}
