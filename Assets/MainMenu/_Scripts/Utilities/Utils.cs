﻿using UnityEngine;

public static class Utils
{
    public static float Map(float val, float oldMin, float oldMax, float newMin, float newMax)
    {
        float clampedVal = Mathf.Clamp(val, oldMin, oldMax);

        if (val != clampedVal)
        {
            Debug.LogWarning("val is not in defined Range. It was changed from " + val + " to " + clampedVal + ".");
            val = clampedVal;
        }

        float oldRange = oldMax - oldMin;
        float newRange = newMax - oldMax;

        return (((val - oldMin) / oldRange) * newRange) + newMin;
    }    
}
