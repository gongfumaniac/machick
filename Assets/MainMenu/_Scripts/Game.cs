﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    private LevelManager levelManager;
    public LevelManager LevelManager
    {
        get { return levelManager; }
    }

    private AudioManager audioManager;
    public AudioManager AudioManager
    {
        get { return audioManager; }
    }




    private static Game instance;
    public static Game Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<Game>();

                Instance.levelManager = Instance.GetComponentInChildren<LevelManager>();
                Instance.audioManager = Instance.GetComponentInChildren<AudioManager>();


                DontDestroyOnLoad(instance.gameObject);
            }
            return instance;
        }
        private set { instance = value; }
    }

    void Awake()
    { 
        if (instance != null && Instance != this)
        {
            Debug.LogWarning("An instance of MenuEvents is already in the scene. This Gameobject is destroyed.");
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        levelManager = GetComponentInChildren<LevelManager>();
        audioManager = GetComponentInChildren<AudioManager>();
    }
}
