﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public const string level01 = "Level01";

    void OnEnable()
    {
        MenuEvents.Instance.ToPlay.AddListener(ChangeScene);

        //LoadLevel(3);
    }
    private void OnDisable()
    {
        MenuEvents.Instance?.ToPlay.RemoveListener(ChangeScene);
    }

    public void ChangeScene()
    {
        //SceneManager.LoadScene(level01);
        SceneManager.LoadScene("Main");
    }
    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }


    //This is custom made and not used. Gives error message if the Level Number doesn't exist in build.
    public void LoadLevel(int levelNumber)
    {
        string levelName = "Level";
        if (levelNumber < 10)
        {
            levelName = levelName + 0;
        }
        levelName = levelName + levelNumber;

        //if (SceneManager.GetSceneByName(levelName).IsValid()) -> only checks active Scenes
        if (Application.CanStreamedLevelBeLoaded(levelName)) //This should be obsolete, but works nonetheless
        {
            SceneManager.LoadScene(levelName);
        }
        else
        {
            Debug.LogError(levelName + " cannot be loaded. It doesn't exist in Build.");
        }
    }
}