﻿using UnityEngine;
using UnityEngine.Events;

public class MenuEvents : MonoBehaviour
{
    public UnityEvent ToPlay = new UnityEvent();
    public UnityEvent<ActiveMenu> ChangeMenu = new ActiveMenuEvent();
    public UnityEvent<float> ChangeVolume = new FloatEvent();
    public UnityEvent PrintUnitValFromVolume = new UnityEvent();
    public UnityEvent PrevTrack = new UnityEvent();
    public UnityEvent NextTrack = new UnityEvent();

    private static MenuEvents instance;
    public static MenuEvents Instance
    {
        get
        {
            if(instance == null)
            {
                instance = GameObject.FindObjectOfType<MenuEvents>();
            }
            return instance;
        }
        private set { instance = value; }
    }

    void Awake()
    {
        if (instance != null && Instance != this)
        {
            Debug.LogWarning("An instance of MenuEvents is already in the scene. This Gameobject is destroyed.");
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void ToPlayButton()
    {
        ToPlay.Invoke();
    }

    public void ToMainMenu()
    {
        ChangeMenu.Invoke(ActiveMenu.Main);
    }

    public void ToOptionsMenu()
    {
        ChangeMenu.Invoke(ActiveMenu.Options);
    }

    public void VolumeSlider(float val)
    {
        ChangeVolume.Invoke(val);
    }

    public void VolToUnitValButton()
    {
        PrintUnitValFromVolume.Invoke();
    }

    public void NextTrackButton()
    {
        NextTrack.Invoke();
    }

    public void PrevTrackButton()
    {
        PrevTrack.Invoke();
    }

}

public enum ActiveMenu
{
    Main = 0,
    Options = 1
}

[System.Serializable]
public class ActiveMenuEvent : UnityEvent<ActiveMenu>
{
}

[System.Serializable]
public class FloatEvent : UnityEvent<float>
{
}