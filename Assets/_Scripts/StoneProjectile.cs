﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneProjectile : MonoBehaviour
{
    float minSize = 0.4f;
    float maxSize = 1.75f;
    float fullyChargedMultiplier = 1.2f;
    float speed = 1200f;
    float sizeMultiplier = 0.4f;

    float minTorque = 5f;
    float maxTorque = 50f;

    float massMultiplier = 3f;

    Rigidbody rigid;


    //dealing damage.
    float minVelocity = 2f;

    float thisSize;
    float damageOutput;
    float damageMultiplier = 11f;

    public List<GameObject> collidedWith;
    bool listEmptied;

    IDamageable<float> otherDamage;

    public float test;


    public void Resize(float size)
    {
        if (size > maxSize)
        {
            size = maxSize * fullyChargedMultiplier;
        }

        thisSize = size;
  
        rigid = this.transform.GetComponent<Rigidbody>();
        rigid.mass = (size * size * size) / 1f;

        this.transform.localScale = Vector3.one * size * size * sizeMultiplier + Vector3.one * minSize;

        Vector3 direction = transform.forward * speed * ((size * size * size * size));
        rigid.AddForce(direction);

        this.transform.eulerAngles = new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));
        rigid.AddTorque(new Vector3(Random.Range(minTorque, maxTorque), Random.Range(minTorque, maxTorque), Random.Range(minTorque, maxTorque)));
    }

    private void FixedUpdate()
    {

        if (rigid.velocity.y > 0.1f)
        {
            rigid.AddForce(Vector3.down * 20f);
        }
        else if (rigid.velocity.y < 0f)
        {
            rigid.AddForce(Vector3.down * 8f);
        }

        test = rigid.velocity.magnitude;
    }

    private void Update()
    {
        if(!listEmptied && rigid.velocity.magnitude < 1f)
        {
            collidedWith.Clear();

            listEmptied = true;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        float collVelocity = other.relativeVelocity.magnitude;   

        if (collVelocity > 2f)
        {
            if (!other.gameObject.CompareTag("Player") && !other.gameObject.CompareTag("Environment") && !collidedWith.Contains(other.gameObject))
            {
                collidedWith.Add(other.gameObject);
                otherDamage = (IDamageable<float>)other.gameObject.GetComponent(typeof(IDamageable<float>));

                if (otherDamage != null)
                {
                    damageOutput = thisSize * damageMultiplier;

                    if(collVelocity < 20f)
                    {
                        damageOutput *= collVelocity / 20f;
                    }

                    otherDamage.TakePhysicalDamage(damageOutput);
                }
            }

        }
    }


}
