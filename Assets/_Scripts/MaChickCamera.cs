﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaChickCamera : MonoBehaviour
{
    float zoomSpeed = 1f;
    float maxZoom = 5f;
    float minZoom = 30f;

    float adjustmentSpeed = 8f;
    float slowAdjustment = 3f;
    float quickAdjustment = 7f;
    float adjustmentChangeRate = 60f;
    float adjustmentStep = 1f;



    Vector3 offset = new Vector3(0f, 20f, -15f);
    Vector3 forwardOffset;


    Transform maChick;
    // Use this for initialization
    void Start()
    {
        maChick = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void FixedUpdate()
    {
 
            SetPosition();

        Zoom();

        //transform.LookAt(maChick);
    }

    private void SetPosition()
    {
        transform.position = Vector3.Lerp(transform.position, maChick.position + offset + forwardOffset, Time.deltaTime * adjustmentSpeed);
    }


    void Zoom()
    {
        if (Input.GetAxis("Mouse ScrollWheel") < 0f) // forward
        {
            if (transform.position.y - maChick.position.y < minZoom)
            {
                offset += transform.forward * -zoomSpeed;

            }
        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            if (transform.position.y - maChick.position.y > maxZoom)
            {
                offset += transform.forward * zoomSpeed;
            }
        }
    }
}
