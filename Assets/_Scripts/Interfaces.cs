﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBurnable<D>
{
    void Burn(D burnDamage);
}

public interface IWettable<D>
{
    void GetWet(D waterDamage);
}

public interface IDamageable<D>
{
    void TakePhysicalDamage(D damageTaken);
}

public interface IHealable<D>
{
    void Heal(D pointsHealed);
}

public interface IKillable
{
    void Die();
}