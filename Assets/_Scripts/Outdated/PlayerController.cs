﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{


    bool stunned;
    bool dead;


    Animator anim;
    Rigidbody rb;
    public Vector3 inputVector;
    float maxSpeed = 14f;
    float accelaration = 25000f;

    float maxTurnSpeed = 1500f;
    float turnSpeed;
    float attackTurnSpeed = 100f;
    float turnRecovery = 1.08f;


    Ray cursorRay;
    RaycastHit cursorHit;

    float rotationSpeed = 1f;

    Transform mesh;
    ParticleSystem fireBreath;
    ParticleSystem spitWater;
    public Transform stoneProjectile;
    float stoneSize = 0f;
    float stoneChargeRate = 1.5f;
    float stoneMinSize = 0.6f;
    bool firing;
    float slowWalkMultiplier = 0.15f;
    float stoneWalkMultiplier = 0.15f;

    enum Elements { Fire, Water, Earth, Wind };
    Elements element;

    Material elementIndicator; //placeholder for testing purposes


    void Start()
    {
        turnSpeed = maxTurnSpeed;
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();


        element = Elements.Fire;

        elementIndicator = transform.GetChild(0).GetComponent<Renderer>().material;
        elementIndicator.color = Color.red;

        mesh = transform.GetChild(2);
        fireBreath = mesh.GetChild(0).GetComponent<ParticleSystem>();
        fireBreath.Stop();
        spitWater = mesh.GetChild(1).GetComponent<ParticleSystem>();
        spitWater.Stop();
    }

    private void FixedUpdate()
    {
        //Applies the inputVector to move.
        if (rb.velocity.magnitude < maxSpeed && !dead)
        {
            rb.AddForce(inputVector * accelaration * Time.deltaTime);
        }
    }

    void Update()
    {
        if (firing && !Input.GetButton("Fire1"))
        {
            AfterAttack();
        }

        if (stunned)
        {
            //ResetInput(); ResetAttacks();   and maybe reset velocity.
        }
        else if (Input.GetButton("Fire1"))
        {
            FrontAttack();
        }
        else if (Input.GetButton("Fire3"))
        {
            InwardAttack();

            //ResetInput(); - not sure yet if I want MaChick to be able to move while doing an inward attack.
        }
        else if (Input.GetButton("Fire2"))
        {
            BackAttack();
            ResetInput();
        }
        else
        {
            Movement(1f);
            Turn(turnSpeed);
            SetElement();
        }

    }



    void FrontAttack()
    {
        anim.SetBool("Slow", true);
        anim.SetBool("BeakOpened", true);


        firing = true;
        turnSpeed = attackTurnSpeed * 1.2f;
        Turn(attackTurnSpeed);




        if (element == Elements.Fire)
        {
            FireBreath();
        }
        else if (element == Elements.Water)
        {
            SpitWater();
        }
        else if (element == Elements.Earth)
        {
            ChargeStone();


            //ResetInput();
        }
    }


    private void FireBreath()
    {
        fireBreath.Play();
        Movement(slowWalkMultiplier);
    }
    private void SpitWater()
    {
        spitWater.Play();
        Movement(slowWalkMultiplier);
    }
    private void ChargeStone()
    {
        stoneSize += Time.deltaTime * stoneChargeRate;
        if (stoneSize > 1.5f) print("stone is fully charged");

        if (stoneWalkMultiplier > 0.02f)
        {
            stoneWalkMultiplier -= 0.001f;
        Movement(stoneWalkMultiplier);

        }
        else
        {
            anim.SetBool("Walking", false);
        }
    }


    private void InwardAttack()
    {
        print("InwardAttack");
    }

    private void BackAttack()
    {
        print("BackAttack");
    }

    void Movement(float speedMultiplier)
    {
        WalkAnimations();

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        inputVector = new Vector3(h, 0f, v).normalized * speedMultiplier;
    }



    void Turn(float speed)
    {
        cursorRay = Camera.main.ScreenPointToRay(Input.mousePosition);


        if (Physics.Raycast(cursorRay, out cursorHit))
        {
            Transform target = transform.GetChild(1);
            target.position = cursorHit.point;
            target.position = new Vector3(target.position.x, transform.position.y, target.position.z);
            target.LookAt(transform, Vector3.up);
            target.eulerAngles += new Vector3(0f, 180f, 0f);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, target.rotation, Time.deltaTime * speed);
        }

        //Increases Turn speed gradually instead of snapping back to 100% after using ability.
        if (turnSpeed > attackTurnSpeed && turnSpeed < maxTurnSpeed)
        {
            turnSpeed *= turnRecovery;

        }
        else if (turnSpeed > maxTurnSpeed)
        {
            turnSpeed = maxTurnSpeed;
        }
    }





    private void WalkAnimations()
    {
        if (inputVector != Vector3.zero)
        {
            anim.SetBool("Walking", true);
        }
        else
        {
            anim.SetBool("Walking", false);
        }

        //Here I use the velocity of rb in local space and normalize it to make it usable for the animator.
        float v;
        float h;

        Vector3 local;
        Vector3 normalized;

        local = transform.InverseTransformDirection(rb.velocity);
        normalized = new Vector3(local.z, 0f, local.x).normalized;
        v = normalized.x;
        h = normalized.z;

        anim.SetFloat("VelocityV", v, 1f, Time.deltaTime * 30f);
        anim.SetFloat("VelocityH", h, 1f, Time.deltaTime * 30f);
    }

    void ResetInput()
    {
        if (inputVector != Vector3.zero)
        {
            inputVector = Vector3.zero;
        }
    }

    private void AfterAttack()
    {
        anim.SetBool("BeakOpened", false);
        anim.SetBool("Slow", false);

        if (element == Elements.Fire)
        {
            fireBreath.Stop();
        }
        if (element == Elements.Water)
        {
            spitWater.Stop();
        }
        if (element == Elements.Earth)
        {
            if (stoneSize < stoneMinSize)
            {
                //Cough dust
            }
            else
            {
                Vector3 projectilePosition = transform.position + transform.forward * 2.5f + Vector3.up * 1f;
                Transform shotStoneProjectile = GameObject.Instantiate(stoneProjectile, projectilePosition, transform.rotation) as Transform;
                shotStoneProjectile.GetComponent<StoneProjectile>().Resize(stoneSize);

            }
            stoneSize = 0f;
            stoneWalkMultiplier = slowWalkMultiplier;
        }
        firing = false;
    }

    void SetElement()
    {
        if (Input.GetButtonDown("Element1"))
        {
            element = Elements.Fire;
            elementIndicator.color = Color.red;

        }
        else if (Input.GetButtonDown("Element2"))
        {
            element = Elements.Water;
            elementIndicator.color = Color.blue;

        }
        else if (Input.GetButtonDown("Element3"))
        {
            element = Elements.Earth;
            elementIndicator.color = Color.yellow;

        }
        else if (Input.GetButtonDown("Element4"))
        {
            element = Elements.Wind;
            elementIndicator.color = Color.grey;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        SmoothWalk(other);
    }

    void SmoothWalk(Collision other)
    {
        if (other.contacts[0].normal.y < 0.5f)
        {
            transform.Translate(other.contacts[0].normal * 0.025f);
        }
    }
}


