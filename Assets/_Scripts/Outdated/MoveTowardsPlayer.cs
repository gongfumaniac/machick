﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsPlayer : MonoBehaviour
{
    Transform player;
    Vector3 moveDirection;
    Vector3 lookTarget;

    float yPos = -10f;

    float startY;

    public float speed;

    BasicHealth playerHealth;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        startY = transform.position.y;
        playerHealth = player.GetComponent<BasicHealth>();
    }


    void FixedUpdate()
    {
        if(playerHealth.healthPoints <= 0) { return; }

        lookTarget = player.position;
        lookTarget.y = transform.position.y;
        transform.rotation = Quaternion.LookRotation((player.position - transform.position), Vector3.up);

        moveDirection = player.position - transform.position;
        moveDirection.y = 0f;
        transform.Translate(moveDirection.normalized * Time.deltaTime * speed * 1f, Space.World);
        transform.position = new Vector3(transform.position.x, startY, transform.position.z);

        Vector3 pos = transform.position;
        pos.y = startY;
        transform.position = pos;
    }
}
