﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Burnable_TEST : MonoBehaviour, IBurnable<float>
{
    Renderer rend;
    float green;


    void Start()
    {
        rend = GetComponent<Renderer>();

        green = 1f;
    }

    void Update()
    {
        if(green < 1f)
        {
            green += 0.005f;
        }
        rend.material.color = new Color(0f, green, 0f);
    }

    public void Burn(float burnDamage)
    {
        if(green > 0.3f)
        {
            green -= 0.15f;
        }
        else
        {
            green = 0f;
        }
    }
}
