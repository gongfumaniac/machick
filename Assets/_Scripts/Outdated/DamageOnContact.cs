﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnContact : MonoBehaviour
{

    IDamageable<float> otherDamageable;

    public float damage;

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            otherDamageable = (IDamageable<float>)other.gameObject.GetComponent(typeof(IDamageable<float>));
            if (otherDamageable != null)
            {
                otherDamageable.TakePhysicalDamage(damage);
            }

        }

    }
}

