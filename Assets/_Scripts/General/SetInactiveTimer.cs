﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetInactiveTimer : MonoBehaviour
{
    public float secondsUntilInactive;

    [HideInInspector]
    public float timer;


    void FixedUpdate()
    {
        timer += Time.deltaTime;

        if(timer > secondsUntilInactive)
        {
            gameObject.SetActive(false);
        }
    }
}
