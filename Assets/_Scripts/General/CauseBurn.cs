﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CauseBurn : MonoBehaviour
{
    public float damage;

    IBurnable<float> otherBurnable;

    private void Start()
    {
        if(damage <= 0f)
        {
            Debug.LogError("This object deals " + damage + " burn damage.");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        otherBurnable = (IBurnable<float>)other.gameObject.GetComponent(typeof(IBurnable<float>));
        if(otherBurnable != null)
        {
            otherBurnable.Burn(damage);
        }
    }
}
