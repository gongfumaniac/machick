﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explodable : MonoBehaviour, IBurnable<float>
{
    //remember to pool the explosion prefab!

    [SerializeField]
    bool debugSphere = false;

    float healthPoints = 10f;
    public float maxExplodeResistance = 15f;
    float explosionRadius = 10f;
    float explosionDamge = 8f;
    float resistanceRegen = 0.1f;
    float explodeResistance;


    public Transform explosion;

    private void Start()
    {
        explodeResistance = maxExplodeResistance;
    }

    public void Burn(float burnDamage)
    {
        explodeResistance -= burnDamage;

        if(explodeResistance <= 0f)
        {
            Explode();
        }
    }
    public void Damaged(float damage)
    {
        healthPoints = damage;

        CheckHP();
    }

    private void CheckHP()
    {
        if (healthPoints <= 0f)
        {
            gameObject.SetActive(false);
        }
    }

    private void Explode()
    {
        //This should be pooled instead.
        Instantiate(explosion, transform.position, Quaternion.identity);
        gameObject.SetActive(false);

        Collider[] hitColls = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach(Collider c in hitColls)
        {
            IBurnable<float> burnable = (IBurnable<float>)c.GetComponent(typeof(IDamageable<float>));
            if(burnable != null)
            {
                burnable.Burn(explosionDamge);
            }
        }
    }

    private void Update()
    {
        if (explodeResistance < maxExplodeResistance)
            explodeResistance += resistanceRegen * Time.deltaTime;
    }

    private void OnDrawGizmos()
    {
        if (debugSphere)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, explosionRadius);
        }
    }
}
