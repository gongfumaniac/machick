﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class BasicHealth : MonoBehaviour, IBurnable<float>, IWettable<float>,  IDamageable<float>, IHealable<float>
{
    //O should instantiate a 'Burning' particle effect which can play if(burning > 0f).

    //I need to create a connection to the level in every attack script.

    //Die() needs to be worked on.
    //Deactivate damaging hitbox on death.
    //I may want to call a death script by creating a new interface. Variations could be 

    //The isPlayer-boolean may be unnecessary.

    [Tooltip("The healthPoints, burningResistance and damage-output values will be multiplied by 1.2 per level.")]
    public float level = 1f;
    public float healthPoints;
    float levelScaleFactor = 1.2f;

    float maxHealth;



    [Header("Weaknesses")]
    [Tooltip("Standard Value is 1. If it is set to a minus Value Character will be healed by damage.")]
    public float physicalWeakness = 1f;

    [Tooltip("Standard Value is 1.")]
    public float fireWeakness = 1f;
    [Tooltip("Standard value is 15. How difficult it is to apply DoT.")]
    public float burningResistance = 15f;
    [Tooltip("Standard Value is 10. Damage per second.")]
    public float burningDpS = 10f;
    [Tooltip("Standard Value is 6. If this is set to 0 this Character cannot burn.")]
    public float burningDuration = 6f;

    [Tooltip("Standard Value is 0.")]
    public float waterWeakness = 0f;
    [Tooltip("Standard value is 5. How difficult it is to apply Debuff.")]
    public float wetResistance = 5f;
    [Tooltip("Standard Value is 5. If set to 0 this Character cannot be wet.")]
    public float maxWetDuration = 5f;

    [Header("Healing")]
    [Tooltip("Determines how much character is affected by healing. IMPORTANT: If this is < 0, regeneration percantage has to be too!")]
    public float healMultiplier = 1f;
    [Tooltip("How many seconds until Regeneration starts after taking damage.")]
    public float regenerationDelay = 5f;

    float regenerationTimer;

    [Tooltip("Percent of HP regenerated per second.")]
    public float regenerationPercentage = 10f;


    [Tooltip("Starts counting after Death. How long until visibility reaches 0. In Seconds.")]
    public float timeUntilDespawn = 1f;

    [Tooltip("How long. In Seconds.")]
    public float fadeTime = 1f;

    [Header("UI")]
    [Tooltip("If UI should be shown before damage is taken.")]
    public bool hideUI = true;
    [Tooltip("Uncheck if HealthBar and canvas are already there.")]
    public bool createNewUI = true;
    [Tooltip("Multiplicator for the size of the canvas. Usually keep at 1.")]
    public float canvasSize = 1f;
    [Tooltip("0 is average human height. Set to minus value if this character is smaller.")]
    public float canvasHeightOffset = 0f;


    //CreateNewUI
    GameObject enemyUI;
    public GameObject canvas;

    //public Object[] allUI;
    public List<GameObject> allUI;

    


    Image healthBar;


    float wet = 0f;
    float maxWetResistance;

    float wetBuff = 0.8f; //reduces fire damage if wet.
    float dryFactor = 0.2f; //multiplied by burn damage to reduce wet debuff.

    float burning;
    float maxBurningResistance;
    float burnRecoveryRate = 2f; //How many seconds it takes for the burning Resistance to be recovered.

    float wetRecovery;
    float wetRecoveryRate = 1f;

    bool isPlayer = false;


    //Colliders
    BoxCollider[] boxColliders;
    SphereCollider[] sphereColliders;
    CapsuleCollider[] capsuleColliders;
    MeshCollider[] meshColliders;
    

    void Start()
    {
        GetColliders();

        GetLevel();

        if (createNewUI)
        {
            CreateNewUI();
        }
        else
        {
            foreach (Transform child in transform)
            {
                if (child.CompareTag("UI"))
                {
                    canvas = child.gameObject;
                }
            }
            FindHealthBar();
        }

        if (hideUI)
        {
            canvas.gameObject.SetActive(false);
        }

        if (this.gameObject.CompareTag("Player"))
        {
            isPlayer = true;
        }

        GetMaxValues();
    }

    private void GetColliders()
    {
        boxColliders = GetComponentsInChildren<BoxCollider>();
        sphereColliders = GetComponentsInChildren<SphereCollider>();
        capsuleColliders = GetComponentsInChildren<CapsuleCollider>();
        meshColliders = GetComponentsInChildren<MeshCollider>();
    }

    private void GetLevel()
    {
        healthPoints *= Mathf.Pow(levelScaleFactor, (level - 1f));
        burningResistance *= Mathf.Pow(levelScaleFactor, (level - 1f));
    }

    private void CreateNewUI()
    {
        enemyUI = Resources.Load("EnemyUI") as GameObject;
        canvas = Instantiate(enemyUI, transform) as GameObject;
        canvas.transform.localScale *= canvasSize;

        Vector3 canvasOffset = Vector3.zero;
        canvasOffset.y = canvasHeightOffset;
        canvas.transform.position += canvasOffset;

        FindHealthBar();
    }

    private void FindHealthBar()
    {
        if (healthBar == null)
        {
            foreach (Transform child in canvas.transform)
            {
                if (child.CompareTag("HealthBar"))
                {
                    healthBar = child.GetComponent<Image>();
                    break;
                }
                else
                {
                    foreach (Transform grandChild in child)
                    {
                        if (grandChild.CompareTag("HealthBar"))
                        {
                            healthBar = grandChild.GetComponent<Image>();
                            break;
                        }
                    }
                }
            }
        }
    }

    private void GetMaxValues()
    {
        maxBurningResistance = burningResistance;
        maxWetResistance = wetResistance;
        maxHealth = healthPoints;
    }

    void FixedUpdate()
    {
        // put all of this in a StatusCheck() function.
        if (regenerationTimer > 0f)
        {
            regenerationTimer -= Time.deltaTime;
        }


        if (healthPoints < maxHealth && regenerationTimer <= 0f)
        {

            float regeneration = maxHealth * regenerationPercentage * Time.deltaTime;
            Heal(regeneration);

        }

        if (wet > 0f)
        {
            wet -= Time.deltaTime;
        }
        else if (wet < 0f)
        {
            wet = 0f;
        }
        else if (wetResistance < maxWetResistance)
        {
            wetResistance += maxWetResistance * (1 / wetRecoveryRate) * Time.deltaTime;
        }
        else if (wetResistance > maxWetResistance)
        {
            wetResistance = maxWetResistance;
        }


        if (burning > 0f)
        {
            float burningDamage = burningDpS * fireWeakness * Time.deltaTime;

            if (burningDamage > 0f)
            {
                ResetRegenerationTimer();
            }

            burning -= Time.deltaTime;

            UpdateHP(-burningDamage);
        }
        else if (burning < 0f)
        {
            burning = 0f;
        }
        else if (burningResistance < maxBurningResistance)
        {
            burningResistance += maxBurningResistance * (1 / burnRecoveryRate) * Time.deltaTime;
        }
        else if (burningResistance > maxBurningResistance)
        {
            burningResistance = maxBurningResistance;
        }
    }

    public void Burn(float burnValue)
    {
        float fireDamage = burnValue * fireWeakness;

        if (fireDamage > 0f)
        {
            ResetRegenerationTimer();
        }


        if (wet > 0f)
        {
            fireDamage *= wetBuff;
            wet -= burnValue * dryFactor;
        }
        else
        {
            burningResistance -= burnValue;

            if (burningResistance == 0f)
            {
                burning = burningDuration;
            }
            else if (burningResistance < 0f)
            {
                burning = burningDuration;

                burningResistance = 0f;
            }
        }


        UpdateHP(-fireDamage);
    }

    public void GetWet(float waterValue)
    {
        float waterDamage = waterValue * waterWeakness;

        if (waterDamage > 0f)
        {
            ResetRegenerationTimer();
        }

        wetResistance -= waterValue;
        if (wetResistance <= 0f)
        {
            wet += waterValue;

            if (burning > 0f)
            {
                burning = 0f;
            }

            if (wet > maxWetDuration)
            {
                wet = maxWetDuration;
            }

        }
        UpdateHP(-waterDamage);
    }

    public void TakePhysicalDamage(float damage)
    {
        float physDamage = damage * physicalWeakness;

        if (physDamage > 0f)
        {
            ResetRegenerationTimer();
        }

        UpdateHP(- physDamage);
    }

    public void Heal(float healing)
    {
        UpdateHP(healing);
    }

    private void UpdateHP(float updateValue)
    {
        healthPoints += updateValue;


        if (hideUI)
        {
            canvas.gameObject.SetActive(true);

            hideUI = false;
        }


        healthBar.fillAmount = healthPoints / maxHealth;

        if(healthPoints > maxHealth)
        {
            healthPoints = maxHealth;
        }
        if (healthPoints <= 0f)
        {
            Die();
        }
    }

    void ResetRegenerationTimer()
    {
        regenerationTimer = regenerationDelay;
    }

    private void Die()
    {
        //call death script
        //-> reduce visibility, call a 'item drop'-script, then Destroy (or set inactive.)

        // I have to set damaging hitboxes to false.
        //I also have to make sure the AI stops working.

        //I have to set a 'IsDead' bool in the animator to true, if it exists.
        DisableHitboxes();


        IKillable otherKillable = (IKillable)GetComponent(typeof(IKillable));
        if(otherKillable != null)
        {
            otherKillable.Die();
        }
            

        if (isPlayer)
        {
            //Reload Level or sth.

            //This is a placeholder:
            canvas.transform.GetComponentInChildren<Text>(true).gameObject.SetActive(true);
            canvas.transform.parent = null;
            gameObject.SetActive(false);
        }
    }

    private void DisableHitboxes()
    {
        foreach(BoxCollider coll in boxColliders)
        {
            coll.enabled = false;
        }
        foreach (SphereCollider coll in sphereColliders)
        {
            coll.enabled = false;
        }
        foreach (CapsuleCollider coll in capsuleColliders)
        {
            coll.enabled = false;
        }
        foreach (MeshCollider coll in meshColliders)
        {
            coll.enabled = false;
        }
    }
}
