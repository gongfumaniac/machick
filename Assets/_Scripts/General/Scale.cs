﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale : MonoBehaviour
{
    public Vector3 startSize;
    public AnimationCurve curve;

    [Header("Axis to scale")]
    public bool x;
    public bool y;
    public bool z;


    [Header("Hitbox")]
    [Tooltip("Only possible with BoxCollider.")]
    public bool onlyHitBox;
    [Tooltip("Only possible with SphereCollider.")]
    public bool onlyHitSphere;

    BoxCollider boxColl;
    SphereCollider sphereColl;

    float timer;

    Vector3 size;

    SetInactiveTimer inactiveTimer;

    float scale;

    // Use this for initialization

    void Start()
    {
        inactiveTimer = GetComponent<SetInactiveTimer>();

        if (onlyHitBox)
        {
            boxColl = GetComponent<BoxCollider>();
        }
        else if (onlyHitSphere)
        {
            sphereColl = GetComponent<SphereCollider>();
        }
    }

    public void OnSpawn()
    {

        transform.localScale = startSize;

        size = startSize;

        timer = 0f;

        if (inactiveTimer != null)
        {
            inactiveTimer.timer = 0f;
        }
    }

    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;

        scale = curve.Evaluate(timer);


        if (onlyHitSphere)
        {
            sphereColl.radius = scale;
        }
        else if (onlyHitBox)
        {
            SetSizeVector();
            boxColl.size = size;
        }
        else
        {
            SetSizeVector();
            transform.localScale = size;
        }

    }

    private void SetSizeVector()
    {
        if (x)
        {
            size.x = scale;
        }
        if (y)
        {
            size.y = scale;
        }
        if (z)
        {
            size.z = scale;
        }
    }
}
