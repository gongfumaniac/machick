﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaChick_Controller : MonoBehaviour
{
    //part of pooler
    ObjectPooler objectPooler;
    float attackTimer;           //It needs to be set equal to attack interval if attack should start immediately after turning.
    float attackInterval = 0.1f;  

    //unorganized
    bool stunned;
    bool firing;
    Vector3 lookatVector;

    bool add;
    bool substract;

    float beakTimer;


    public Material elementIndicator; //Placeholder until UI is established.

    Animator anim;

    //Elements 
    enum Elements { Fire, Water, Earth, Wind };
    Elements element;

    //input
    float v;
    float h;
    public Vector3 inputVector;

    Transform movementTarget;

    //Movement
    Rigidbody rb;
    float quickAcceleration = 350f;
    public float acceleration;
    float maxSpeed = 35f;

    float slowAcceleration = 100f;
    float slowMaxSpeed = 10f;

    float accelerationChange = 2f;

    //reticle
    Transform reticle;

    Vector3 reticleOffset;
    Vector3 reticlePosition;

    Ray cursorRay;
    RaycastHit cursorHit;

    //particles
    Transform mesh;

    ParticleSystem flameBreath;
    ParticleSystem waterSpit;


    //turning
    float turnSpeed = 280f;
    float slowTurnMultiplier = 0.5f;
    float turnSpeedMultiplier = 2f;
    float modifiedSpeed;
    public float rotationDifference;




    //front attack
    bool resetCondition;

    //front attack STONE
    public Transform stoneProjectile;
    float stoneSize = 0f;
    float stoneChargeRate = 1.5f;
    float stoneMinSize = 0.6f;

    float selfKnockback = 3000f;
    float speedStep_stone = 0.5f;

    void Start()
    {
        attackTimer = attackInterval;

        objectPooler = ObjectPooler.Instance;


        mesh = transform.GetChild(0);
        reticle = transform.GetChild(1);
        movementTarget = transform.GetChild(2);

        flameBreath = mesh.GetChild(0).GetComponent<ParticleSystem>();
        flameBreath.Stop();

        waterSpit = mesh.GetChild(1).GetComponent<ParticleSystem>();
        waterSpit.Stop();

        rb = GetComponent<Rigidbody>();

        anim = GetComponent<Animator>();

        acceleration = quickAcceleration;

        element = Elements.Fire;

        //elementIndicator = transform.GetChild(5).GetComponent<Renderer>().material;
        elementIndicator.color = Color.red;
    }

    void Update()
    {
        SetInput();

        if (firing && !Input.GetButton("Fire1")) //there may be a better position for this / a better way to check for this.
        {
            AfterAttack();
        }

        SetMovementTargetPosition(); //bad name and not at the right position yet.

        if (stunned)
        {
            //ResetInput(); ResetAttacks();   and maybe reset velocity.
        }
        else if (Input.GetButton("Fire1"))
        {
            FrontAttack();
        }
        else if (Input.GetButtonDown("Fire2"))
        {
            //InwardAttack();
            //ResetInput(); - not sure yet if I want MaChick to be able to move while doing an inward attack.
            //-> could set stunned for half a second but will have to watch out for animator if stunned overrides every other animation.
        }
        else if (Input.GetButton("Fire3"))
        {
            // BackAttack();
            // ResetInput();
        }
        else
        {
            if (acceleration != quickAcceleration && !add && !substract)
            {
                StartCoroutine(ChangAcceleration(acceleration, quickAcceleration, 2f));
            }


            TurnTowards(movementTarget, turnSpeed);
            Movement(acceleration);

            //Turn(turnSpeed);
            SetElement();
        }
    }

    private void AfterAttack()
    {
        firing = false;

        attackTimer = attackInterval;

        //This is an alternative to checking the currently attuned element.
        //if (fireBreath.isPlaying) fireBreath.Stop();
        //if (waterSpit.isPlaying) waterSpit.Stop();

        if (element == Elements.Fire)
        {
            flameBreath.Stop();
            anim.SetBool("Firing", false);
        }
        if (element == Elements.Water)
        {
            waterSpit.Stop();
            anim.SetBool("Firing", false);
        }
        if (element == Elements.Earth)
        {
            anim.SetBool("Firing", true);
            StartCoroutine(CloseBeak(0.5f));

            SpitStone();

        }

        //if earth is active throw stone
    }

    IEnumerator CloseBeak(float timeUntilChange)
    {
        while (beakTimer < timeUntilChange)
        {
            beakTimer += Time.deltaTime;

            yield return null;
        }

        beakTimer = 0f;

        if (!firing)
        {
            anim.SetBool("Firing", false);
        }
    }

    private void FrontAttack()
    {
        // anim.SetBool("Slow", true);        //anim.SetBool("BeakOpened", true); -> later when attack is actually performed

        SetReticle();

        //Changes the walk speed.
        if (!add && !substract)
        {
            if (element == Elements.Earth)
            {
                StartCoroutine(ChangAcceleration(acceleration, 0f, speedStep_stone));
            }
            else
            {
                StartCoroutine(ChangAcceleration(acceleration, slowAcceleration, 2f));
            }
        }
        Movement(acceleration);


        if (firing)
        {
            TurnTowards(reticle, turnSpeed * slowTurnMultiplier);

            if (element == Elements.Fire)
            {
                BreatheFire();
                flameBreath.Play();//check if it is playing beforehand.
            }
            else if (element == Elements.Water)
            {
                SpitWater();
            }
            else if (element == Elements.Earth)
            {
                ChargeStone();
            }
            else if (element == Elements.Wind)
            {
                //wind attack
            }
        }
        else
        {
            TurnTowards(reticle, turnSpeed * 1.5f);

            if (rotationDifference < 5f)
            {
                firing = true;
            }

            if (element == Elements.Earth)
            {
                ChargeStone();
            }
            //if earth the charging should already start
        }
    }




    private void BreatheFire()
    {
        flameBreath.Play();
        if (anim.GetBool("Firing") == false) anim.SetBool("Firing", true);
        //enable lights

        //the following lines spawns hitboxes from a pool.

        attackTimer += Time.deltaTime;

        if (attackTimer >= attackInterval)
        {
            GameObject obj = objectPooler.SpawnFromPool("FlameBreath", flameBreath.gameObject.transform.position + transform.forward, transform.rotation);
            obj.SetActive(true);
            obj.GetComponent<Scale>().OnSpawn();
            attackTimer -= attackInterval;
        }
    }

    private void SpitWater()
    {
        waterSpit.Play();
        if (anim.GetBool("Firing") == false) anim.SetBool("Firing", true);
    }

    private void ChargeStone()
    {
        stoneSize += Time.deltaTime * stoneChargeRate;
    }

    private void SpitStone()
    {
        if (stoneSize < stoneMinSize)
        {
            //Cough dust
        }
        else
        {
            Vector3 projectilePosition = transform.position + transform.forward * 2.5f + Vector3.up * 1f;
            Transform shotStoneProjectile = GameObject.Instantiate(stoneProjectile, projectilePosition, transform.rotation) as Transform;
            shotStoneProjectile.GetComponent<StoneProjectile>().Resize(stoneSize);

        }
        stoneSize = 0f;

        rb.AddForce(-transform.forward * selfKnockback);
    }

    IEnumerator ChangAcceleration(float currentAcellaration, float targetAcelleration, float step)
    {

        if (!add && !substract)
        {
            if (currentAcellaration < targetAcelleration) add = true;
            else if (currentAcellaration > targetAcelleration) substract = true;
        }

        while (add)
        {
            if (acceleration >= targetAcelleration)
            {
                acceleration = targetAcelleration;

                add = false;
                yield break;
            }
            acceleration += quickAcceleration * accelerationChange * Time.deltaTime * step;

            yield return null;
        }

        while (substract)
        {
            if (acceleration <= targetAcelleration)
            {
                acceleration = targetAcelleration;

                substract = false;
                yield break;
            }
            acceleration -= quickAcceleration * accelerationChange * Time.deltaTime * step;

            yield return null;
        }

    }

    private void SetInput()
    {
        h = Input.GetAxisRaw("Horizontal");
        v = Input.GetAxisRaw("Vertical");
        inputVector = new Vector3(h, 0f, v);


    }

    private void SetReticle()
    {
        cursorRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(cursorRay, out cursorHit))
        {
            reticle.position = cursorHit.point;
            reticle.position = new Vector3(reticle.position.x, transform.position.y, reticle.position.z);
            reticle.LookAt(transform, Vector3.up);
            reticle.eulerAngles += new Vector3(0f, 180f, 0f);

            //transform.rotation = Quaternion.RotateTowards(transform.rotation, reticle.rotation, Time.deltaTime * speed);
        }
    }



    private void Movement(float maxSpeed)
    {
        WalkAnimations();

        // Turn(speed variable);
        if (rb.velocity.magnitude < maxSpeed && inputVector != Vector3.zero)
        {
            //rb.AddRelativeForce(Vector3.forward * acceleration);
            rb.AddForce(inputVector.normalized * acceleration);
        }
    }

    private void WalkAnimations()
    {
        if (rb.velocity.magnitude > 3f)
        {
            if (anim.GetBool("Walking") == false) anim.SetBool("Walking", true);
        }
        else
        {
            if (anim.GetBool("Walking") == true) anim.SetBool("Walking", false);
        }



        //Here I use the velocity of rb in local space and normalize it to make it usable for the animator.
        float v;
        float h;

        Vector3 normalized;
        Vector3 local;

        local = transform.InverseTransformDirection(rb.velocity);
        normalized = new Vector3(local.z, 0f, local.x).normalized;
        v = normalized.x;
        h = normalized.z;

        anim.SetFloat("VelocityV", v, 1f, Time.deltaTime * 30f);
        anim.SetFloat("VelocityH", h, 1f, Time.deltaTime * 30f);
    }


    private void TurnTowards(Transform target, float speed)
    {
        rotationDifference = transform.eulerAngles.y - target.eulerAngles.y;
        rotationDifference = Mathf.Abs(rotationDifference);

        if (target == movementTarget)
        {


            if (rotationDifference > 100f)
            {
                modifiedSpeed = speed * turnSpeedMultiplier;
            }
            if (modifiedSpeed > speed && rotationDifference < 80f)
            {
                modifiedSpeed = speed;
            }
        }
        else if (modifiedSpeed != speed)
        {
            modifiedSpeed = speed;
        }

        if (target.position != transform.position)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, target.rotation, Time.deltaTime * modifiedSpeed);
        }
    }

    void SetMovementTargetPosition()
    {
        movementTarget.position = transform.position + inputVector * 3f;
        movementTarget.LookAt(transform, Vector3.up);
        movementTarget.eulerAngles += new Vector3(0f, 180f, 0f);
    }

    void SetElement()
    {
        if (Input.GetButtonDown("Element1"))
        {
            element = Elements.Fire;
            elementIndicator.color = Color.red;

        }
        else if (Input.GetButtonDown("Element2"))
        {
            element = Elements.Water;
            elementIndicator.color = Color.blue;

        }
        else if (Input.GetButtonDown("Element3"))
        {
            element = Elements.Earth;
            elementIndicator.color = Color.yellow;

        }
        else if (Input.GetButtonDown("Element4"))
        {
            element = Elements.Wind;
            elementIndicator.color = Color.grey;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        SmoothWalk(other);
    }

    void SmoothWalk(Collision other)
    {
        if (other.contacts[0].normal.y < 0.5f)
        {
            transform.Translate(other.contacts[0].normal * 0.025f);
        }
    }
}


