﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Alignment : MonoBehaviour
{
    ////Makes an UI element always face the camera. This makes it appear more 2D.
    ////When this script is used on a horizonzal bar the fill origin has to be changed to the opposite direction.


    //Transform cam;

    //void Start()
    //{
    //    cam = Camera.main.transform;

    //    //Inverts the Canvas' scale on the x Axis. Makes it seem as if the UI was actually facing the camera instead of away from it.
    //    transform.localScale = Vector3.Scale(transform.localScale, new Vector3(-1f, 1f, 1f));

    //}

    //void LateUpdate()
    //{
    //    transform.eulerAngles = cam.eulerAngles;
    //}


    //Second Tray after this comment:


    Transform cam;

    void Start()
    {
        cam = Camera.main.transform;
    }

    private void LateUpdate()
    {
        transform.forward = -cam.forward;
    }
}
