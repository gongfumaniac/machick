﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathFade : MonoBehaviour, IKillable
{
    public float timeUntilFade = 1f;
    public float fadeTime = 1f;

    public float fadeRate = 1f;

    CanvasGroup canvas;

    Color currentColor;

    float timer;

    bool dead;

    float opacity = 1f;

    public Renderer[] renderers;

    private void Start()
    {
        fadeRate /= 60f;

        renderers = GetComponentsInChildren<Renderer>();
    }


    public void Die()
    {
        dead = true;
        GetComponent<MoveTowardsPlayer>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Collider>().enabled = false;
        SetShaders();
    }

    private void SetShaders()
    {
        foreach (Renderer rend in renderers)
        {
            //Change Rendering Mode to Fade.  Found this method here: 'https://forum.unity-community.de/topic/10035-change-material-renderingmode-on-runtime/'

            Material mat = rend.material;

            mat.SetFloat("_Mode", 2);
            mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            mat.SetInt("_ZWrite", 0);
            mat.DisableKeyword("_ALPHATEST_ON");
            mat.EnableKeyword("_ALPHABLEND_ON");
            mat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            mat.renderQueue = 3000;
        }
    }

    void Update()
    {
        if (dead)
        {
            timer += Time.deltaTime;
        }
        if (timer > timeUntilFade)
        {
            opacity -= fadeRate;

            if (opacity >= 0f)
            {
                foreach (Renderer rend in renderers)
                {
                    //currentColor = rend.material.color;
                    //rend.material.color = new Color(currentColor.r, currentColor.g, currentColor.b, opacity);
                    rend.material.color -= new Color(0f, 0f, 0f, fadeRate);
                }

                if(canvas == null)
                {
                    foreach (Transform child in transform)
                    {
                        if (child.CompareTag("UI"))
                        {
                            canvas = child.GetComponent<CanvasGroup>();
                        }
                    }
                }
                canvas.alpha = opacity;
                
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

    }

}

