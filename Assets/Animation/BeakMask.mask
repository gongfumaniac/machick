%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: BeakMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: MaChick_LOW-Poly
    m_Weight: 0
  - m_Path: rig
    m_Weight: 1
  - m_Path: rig/ORG-Hips
    m_Weight: 0
  - m_Path: rig/ORG-Hips/ORG-Chest
    m_Weight: 0
  - m_Path: rig/ORG-Hips/ORG-Chest/ORG-Head
    m_Weight: 0
  - m_Path: rig/ORG-Hips/ORG-Chest/ORG-Head/ORG-Head_001
    m_Weight: 1
  - m_Path: rig/ORG-Hips/ORG-Chest/ORG-Head/ORG-Head_001/ORG-Head_002
    m_Weight: 1
  - m_Path: rig/ORG-Hips/ORG-Chest/ORG-Head/ORG-Head_003
    m_Weight: 1
  - m_Path: rig/ORG-Hips/ORG-Chest/ORG-Head/ORG-Head_003/ORG-Head_004
    m_Weight: 1
  - m_Path: rig/ORG-Hips/ORG-Wing_L
    m_Weight: 0
  - m_Path: rig/ORG-Hips/ORG-Wing_L/ORG-WingTip_L
    m_Weight: 0
  - m_Path: rig/ORG-Hips/ORG-Wing_L/ORG-WingTip_L/ORG-WingTip2_L
    m_Weight: 0
  - m_Path: rig/ORG-Hips/ORG-Wing_R
    m_Weight: 0
  - m_Path: rig/ORG-Hips/ORG-Wing_R/ORG-WingTip_R
    m_Weight: 0
  - m_Path: rig/ORG-Hips/ORG-Wing_R/ORG-WingTip_R/ORG-WingTip2_R
    m_Weight: 0
  - m_Path: rig/ORG-Leg_L
    m_Weight: 0
  - m_Path: rig/ORG-Leg_L/ORG-Foot_L
    m_Weight: 0
  - m_Path: rig/ORG-Leg_L/ORG-Foot_L/ORG-Toes_L
    m_Weight: 0
  - m_Path: rig/ORG-Leg_R
    m_Weight: 0
  - m_Path: rig/ORG-Leg_R/ORG-Foot_R
    m_Weight: 0
  - m_Path: rig/ORG-Leg_R/ORG-Foot_R/ORG-Toes_R
    m_Weight: 0
  - m_Path: rig/ORG-Tail
    m_Weight: 0
  - m_Path: rig/ORG-Tail/ORG-Tail_001
    m_Weight: 0
  - m_Path: rig/root
    m_Weight: 0
  - m_Path: WGT-root
    m_Weight: 0
